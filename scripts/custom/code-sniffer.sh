#!/bin/bash

composer global require drupal/coder
export PATH="$PATH:$HOME/composer/vendor/bin"
phpcs --config-set installed_paths ~/.composer/vendor/drupal/coder/coder_sniffer
phpcs -p -s --standard=Drupal sites/all/modules/